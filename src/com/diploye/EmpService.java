package com.diploye;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

class MyException extends Exception {

	public MyException(String msg) {

		super(msg);
	}
}

public class EmpService {

	Employee emp = new Employee();

	public Employee getEmployee(int eid) throws ClassNotFoundException, SQLException, MyException {
		Employee employee = new Employee();

		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/system", "root", "Welcome123");

		PreparedStatement ps = con.prepareStatement("select*from EMP where employeeNo=?");
		ps.setInt(1, eid);
		ResultSet set = ps.executeQuery();
		if (!set.isBeforeFirst()) {
			throw new MyException("Employee with given ID not found");
		}
		while (set.next()) {
			employee.setEmployeeNo(set.getInt(1));
			employee.setEmployeeName(set.getString(2));
			employee.setEmail(set.getString(3));
			employee.setSalary(set.getDouble(4));
		}

		return employee;
	}

}
